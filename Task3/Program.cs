﻿using System;

namespace Task3
{
    internal class Program
    {
        static void Main(string[] args)
        {           

            int[] arr = new int[15];

            arr.FillArray();

            arr.ShowArray();

            arr.SortAscending();

            arr.ShowArray();
        }
    }
}

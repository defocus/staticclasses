﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    internal static class ExtantionClassForArray
    {
        public static int[] SortAscending(this int[] array)
        {
            int temp = 0;

            for (int i = 0; i < array.Length - 1; i++)
            {
                for (int j = 0; j < array.Length - 1; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
            }

            return array;
        }

        public static void ShowArray(this int[] array)
        {
            foreach (int item in array)
                Console.Write(item + "\t");

            Console.WriteLine("\n" + new string('-', 50));
        }

        public static void FillArray(this int[] array)
        {
            Random rand = new Random();

            for (int i = 0; i < array.Length; i++)
                array[i] = rand.Next(-1000, 1001);
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Task1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            FindAndReplaceManager.FindNext("Hello World!");

            Book.Notes notes = new Book.Notes();

            Console.WriteLine(new string('-', 50));

            notes.WriteNote("Hello World!");
            notes.WriteNote("Hello");
            notes.WriteNote("World!");
            notes.WriteNote("1235");
            notes.WriteNote("Abra-Cadabra");
            notes.WriteNote("Hello123");
            notes.WriteNote("123World!");

            //notes.Note.ShowNotes();

            notes.ShowNotes();
        }
    }
}

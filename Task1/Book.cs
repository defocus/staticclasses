﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    internal class Book
    {
        public class Notes
        {
            List<string> notes = new List<string>();

            public void WriteNote(string note)
            {
                notes.Add(note);
            }
            
            public List<string> Note { get { return notes; } }

            public void ShowNotes()
            {
                foreach (string note in notes)
                    Console.WriteLine(note);
            }
        }
    }
}
